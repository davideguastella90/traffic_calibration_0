import traci
from conf import config

DEFAULT_SUMO_PORT = 8813


def start(port=DEFAULT_SUMO_PORT):
    sumo_binary = "sumo-gui" if config.USE_SUMO_UI else "sumo"

    if config.SIMULATE_COLLISIONS:
        """
        [from sumo documentation]
        'The departure times of all vehicles may be varied randomly by using the option
        --random-depart-offset <TIME>. When this option is used each vehicle receives a
        random offset to its departure time, equidistributed on [0, <TIME>].'
        """
        launch_str = [sumo_binary, "--random-depart-offset", str(5), "--seed", str(port),
                      "--random", "-c", config.scenario.cfg_path,
                      '--collision.mingap-factor', str(config.COLLISION_MINGAP_FACTOR),
                      '--collision.action', config.COLLISION_ACTION, '--collision.stoptime',
                      str(config.COLLISION_STOPTIME)]
    else:
        if config.USE_RANDOMNESS:


            #sumo -v -n SS195.net.xml --route-files SS195.odtrips.rou.xml
            # --duration-log.statistics true -b 25200 -e 28800 --time-to-teleport -1
            # --collision.action warn --step-length 0.5 --routing-algorithm dijkstra
            # --time-to-impatience -1 --additional-files SS195.poly.xml

            #sumo_binary -v -n NET -e END
            launch_str = [sumo_binary, "--random-depart-offset", str(5), "--seed", str(port),
                          "--random",
                          "-n", config.scenario.net_path, "-e", str(config.scenario.sim_range.stop),
                          "-b", str(config.scenario.sim_range.start)]

            """launch_str = [sumo_binary, "--random-depart-offset", str(5), "--seed", str(port),
                          "--random",
                          "-c", config.scenario.cfg_path]"""

            """launch_str = [sumo_binary, "--mesosim",
                          "--meso-junction-control.limited", str(True),
                          "--no-internal-links",
                          "--device.rerouting.threads", str(5),
                          "--step-length", str(1),
                          "--meso-jam-threshold",str(0.4),
                          "--device.rerouting.synchronize", "-c", config.scenario.cfg_path]"""
        else:
            launch_str = [sumo_binary, "-c", config.scenario.cfg_path]

    print(f"launching with: {launch_str}")
    traci.start(launch_str, port=port)


def close():
    traci.close()
