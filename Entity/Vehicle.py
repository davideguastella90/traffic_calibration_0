import random
import string

import traci
import traci.constants as tc

from conf import config
from network.Network import Network
from routing.CustomRouter import CustomRouter


class NullVehicle:
    """ a car with no function used for error prevention """

    def __init__(self):
        pass

    def set_arrived(self, tick):
        pass


class Vehicle:
    # when using SUMO user interface, this color will be used for cars that send information to sensing agent
    CAR_SENDING_INFO_COLOR = [255, 0, 0, 255]
    # when using SUMO user interface, this color will be used for cars that do not send information to sensing agent
    CAR_NOT_SENDING_INFO_COLOR = [255, 255, 0, 255]

    """
    A vehicle in SUMO simulation
    """
    preferences_list = ["min_length", "max_speed", "balanced"]

    """A dictionary where the key is the id of a sensing agent, the value is  a list of integer. 
                Each value represents the time that this vehicle passed in a region observed by 
                a specific sensing agent."""
    time_passed_in_each_region = {}

    def __init__(self, car_id: string):
        self.id = car_id  # car_id of this vehicle (sumo ID)

        # counter for sending information. It is reset to 0 once it is equal to info_frequency
        self.send_info_counter = 0

        self.position = 0  # position of this vehicle (xy)

        # the rounds this car already drove
        self.rounds = 0  # type: int

        self.arrived_tick = -1  # the time instant at which this vehicle arrived at its destination

        # each (sensing agent id, key) indicates the last tick that this car provided information
        # to a specific sensing agent
        self.tick_last_info_sent = 0

        self.current_route_begin_tick = None  # when we started the route

        self.target_id = None  # the target node this car drives to

        self.driver_preference = "balanced"
        self.source_id = None
        self.current_route_id = None
        self.current_router_result = None

        self._time_in_region_buf = {}

        self._time_passed_in_current_region = 0
        self._current_region = []

    def add_to_simulation(self, tick):
        """ Add this car to the simulation through the traci API """
        # From SUMO website: "When a vehicle is added using method add it is not immediately
        # inserted into the network.
        # Only after the next call to simulationStep does the simulation try to insert it (and
        # this may fail when in conflict with other traffic). The result of getIDList only contains
        # vehicles that have been inserted into the network which means the vehicle will not be
        # listed immediately. You can force a vehicle to be inserted instantly by calling the
        # function vehicle.moveTo or vehicle.moveToXY after adding it to the simulation."
        self.current_route_begin_tick = tick
        try:
            route = self.__create_new_route(tick)
            traci.vehicle.add(self.id, route)
            traci.vehicle.subscribe(self.id, config.CARS_SUBSCRIPTIONS)
            return True
        except Exception as e:
            print("error adding: " + str(e))
            # try recursion, as this should normally work
            # self.addToSimulation(tick)
            return False

    def __choose_random_source(self):
        if self.target_id is None:
            # self.source_id = Network.get_random_node_id_of_passenger_edge()
            self.source_id = Network.get_random_from_node_office()
        else:
            self.source_id = self.target_id  # We start where we stopped

    def __choose_random_target(self):
        self.target_id = Network.get_random_from_node_apartment()

    def __create_new_route(self, tick):
        """ Create a new route to a random target and uploads this route to SUMO """
        if self.target_id is None:
            self.source_id = Network.get_random_node_id_of_passenger_edge()
            #self.source_id = Network.get_random_from_node_office()
        else:
            self.source_id = self.target_id  # We start where we stopped
        self.target_id = Network.get_random_node_id_of_passenger_edge()
        #self.target_id = Network.get_random_from_node_apartment()
        self.current_route_id = self.id + "-" + str(self.rounds)

        try:
            if self.driver_preference == "min_length":
                self.current_router_result = \
                    CustomRouter.route_by_min_length(self.source_id, self.target_id)
            elif self.driver_preference == "max_speed":
                self.current_router_result = \
                    CustomRouter.route_by_max_speed(self.source_id, self.target_id)
            else:
                self.current_router_result = \
                    CustomRouter.minimal_route(self.source_id, self.target_id)

            if len(self.current_router_result.route) > 0:
                traci.route.add(self.current_route_id, self.current_router_result.route)
                return self.current_route_id
            if config.VERBOSE_LOG:
                print("vehicle " + str(self.id) + " could not be added, retrying")
            return self.__create_new_route(tick)
        except Exception as _:
            if config.VERBOSE_LOG:
                print("vehicle " + str(self.id) + " could not be added [exception], retrying")
            return self.__create_new_route(tick)

    def on_simstep_performed(self, tick):
        """
        Operation(s) to perform at each simulation step
        """
        self.position = traci.vehicle.getPosition(self.id)  # update my position field

    def get_speed(self):
        """Return the speed (in m/s) of this vehicle within the last step."""
        return traci.vehicle.getSubscriptionResults(self.id)[tc.VAR_SPEED]

    def get_co2emission(self):
        """Return the CO2 emissions (in mg/s) of this vehicle within the last step."""
        return traci.vehicle.getSubscriptionResults(self.id)[tc.VAR_CO2EMISSION]

    def get_angle(self):
        """Return the angle of this vehicle within the last step."""
        return traci.vehicle.getSubscriptionResults(self.id)[tc.VAR_ANGLE]

    def get_acceleration(self):
        """Returns the acceleration in m/s^2 of this vehicle within the last step."""
        return traci.vehicle.getSubscriptionResults(self.id)[tc.VAR_ACCELERATION]

    def get_lane_id(self):

        """Returns the ID of the lane in which this vehicle is situated"""
        return traci.vehicle.getSubscriptionResults(self.id)[tc.VAR_LANE_ID]

    def get_vehicle_type(self):
        traci.vehicle.getTypeID(self.id)

    def set_arrived(self, tick):
        """
        Set the time instant at which this vehicle arrives to its destination
        :param tick:
        :return:
        """
        self.arrived_tick = tick
