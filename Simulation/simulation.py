import datetime

import tqdm
import traci

from Register.VehicleRegistry import VehicleRegistry
from conf import config
from environment.TAZEnvironment import TAZEnvironment


class Simulation(object):
    # the current tick of the simulation
    tick = 0  # type: int

    # last tick time in string format
    ts_last_tick = ""

    # the environment
    env = None  # type: Environment

    @classmethod
    def create_environment(cls,):
        """Create a new virtual environment"""
        cls.env = TAZEnvironment()
        cls.env.read_poly_file()
        cls.env.subscribe_polygons_and_poi()

    @classmethod
    def add_vehicles(cls):
        """Add a new vehicle into the simulation"""
        VehicleRegistry.apply_vehicle_counter()

    @classmethod
    def all_vehicles_arrived(cls):
        """Check if all vehicles in the simulation reached their destination"""
        if len(VehicleRegistry.vehicles) == 0:
            print("all cars reached their destinations")
            return True
        return False

    @classmethod
    def loop(cls):
        """
        Start the simulation loop. This method will end after all simulation steps have been
        performed. In scenario configuration you will find the number of simulation steps
        that are performed.
        """
        for _ in tqdm.tqdm(config.scenario.sim_range):
            # Tell sumo to do one simulation step
            traci.simulationStep()
            cls.ts_last_tick = datetime.datetime.now().strftime(config.DATETIME_STR_FORMAT)
            # perform operations for vehicles (if any)
            VehicleRegistry.do_vehicles_sim_step(cls.tick)
            cls.tick += 1
