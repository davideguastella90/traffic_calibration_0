from environment.Region import Region


class Environment:
    net_boundary = 0
    regions = set()
    min_x = .0
    max_x = .0
    min_y = .0
    max_y = .0

    def __init__(self):
        self.min_x = -46
        self.max_x = 1643
        self.min_y = -61
        self.max_y = 1424

    def add_region(self, region: Region):
        self.regions.add(region)
