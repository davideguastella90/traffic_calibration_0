import pickle

import shapely.geometry.point
from shapely.geometry.polygon import Polygon
from libpysal.cg import Chain, Point

import sumolib
import traci
from environment.OSM.BuildingsFromOSM import osm_way
from network.Network import Network
import numpy as np


class Region:
    """
    A region is a local part of the environment observed by a unique sensing agent. A region also
    contains reference to features such as amenities, metro stations, bus stations, workplaces,
    residential areas, etc.
    """

    def __init__(self, id: str, p: Polygon, centroid: Point):
        self.polygon = p
        self.centroid = centroid
        self.id = id
        self.buildings = []

        x, y = zip(*self.polygon.exterior.coords)
        self.x = x
        self.y = y
        self.xy_boundaries = [traci.simulation.convertGeo(x[i], y[i], fromGeo=True) for i in range(0, len(x))]
        self.xy_polygon = Polygon(self.xy_boundaries)

        # TO SERIALIZE
        self.avg_axes_length = 0
        self.edges = []

    def serialize(self):
        serialized_buildings = list(map(lambda bb: osm_way.serializable(bb), self.buildings))
        centroid = str((self.centroid.x, self.centroid.y))
        x = list(map(lambda xx: str(xx), self.x))
        y = list(map(lambda yy: str(yy), self.y))
        edges_str = list(map(lambda edge: edge.getID(), self.edges))
        return {
            'id': self.id,
            'centroid': centroid,
            'edges': edges_str,
            'x': x,
            'y': y,
            'buildings': serialized_buildings
        }

    @staticmethod
    def deserialize(net, regio_dict):
        x = regio_dict['x']
        y = regio_dict['y']
        centroid = Point(eval(regio_dict['centroid']))
        coords = [(float(x[i]), float(y[i])) for i in range(0, len(x))]
        reg = Region(regio_dict['id'], Polygon(coords), centroid)

        edge_lengths = []
        for edge_id in regio_dict['edges']:
            the_edge = net.getEdge(edge_id)
            reg.edges.append(the_edge)
            edge_lengths.append(the_edge.getLength())

        reg.avg_axes_length = np.mean(edge_lengths)

        for building_dict in regio_dict['buildings']:
            building = osm_way.deserialize(building_dict, net)
            reg.add_building(building)
        return reg

    @staticmethod
    def serialize_regions(regions, fname="regions.pickle"):
        ll = list(map(lambda region: region.serialize(), regions))
        with open(fname, "wb") as pickle_out:
            pickle.dump(ll, pickle_out)

    @staticmethod
    def deserialize_regions(net, fname="regions.pickle"):
        regions = []
        dict_pickle_in = pickle.load(open(fname, "rb"))
        for region in dict_pickle_in:
            regions.append(Region.deserialize(net, region))
        return regions

    def add_building(self, building: osm_way):
        self.buildings.append(building)

    def get_num_points(self):
        return len(self.get_shape())

    def get_shape(self):
        return list(self.polygon.exterior.coords)

    def get_shape_nongeo(self):
        return list(self.xy_polygon.exterior.coords)

    def get_boundaries(self):
        x, y = zip(*self.polygon.exterior.coords)
        return x, y

    def get_id(self):
        return self.id

    def get_as_pysal_chain(self):
        shapely_points = list(map(lambda pp: Point([pp[0], pp[1]]), self.get_shape()))
        return [Chain(shapely_points)]

    def __repr__(self):
        return f'\'{self.id}\'; centroid: ({self.centroid.x},{self.centroid.y})'

    def contains_node(self, point: Point):
        return shapely.geometry.point.Point(point[0], point[1]).within(self.polygon)

    def contains_node_xy(self, point: Point):
        x, y = traci.simulation.convertGeo(point[1], point[0], fromGeo=True)
        return shapely.geometry.point.Point(x, y).within(self.xy_polygon)

    def evaluate_edges_in_this_region(self, net):
        inner_edges = list(filter(lambda edge: self.contains_edge(edge.getID()), net.getEdges()))
        edge_lengths = []
        for edge in inner_edges:
            self.edges.append(edge)
            edge_lengths.append(edge.getLength())
        self.avg_axes_length = np.mean(edge_lengths)

    def get_average_edges_occupation(self):

        pass

    def contains_edge(self, edge_id):
        edge = Network.net.getEdge(edge_id)
        edge_boundaries = edge.getShape(includeJunctions=False)

        is_within = False
        for boundary in edge_boundaries:
            is_within = is_within or sumolib.geomhelper.isWithin(boundary, self.get_shape_nongeo())

        return is_within
