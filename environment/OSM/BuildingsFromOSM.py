import pickle
import xml.etree.ElementTree
from dataclasses import dataclass, field

import numpy
import shapely.geometry as geometry
import sumolib
from sumolib.net.edge import Edge
from tqdm import tqdm
from libpysal.cg import Point

from conf import config
from environment.OSM.BuildingType import BuildingType
from os import path


@dataclass(eq=True, frozen=True, order=True, unsafe_hash=True)
class osm_node:
    id: str
    lat: str
    lon: str
    version: str
    timestamp: str
    changeset: str
    uid: str
    user: str

    @staticmethod
    def serialize(node):
        return {'id': node.id,
                'lat': node.lat,
                'lon': node.lon,
                'version': node.version,
                'timestamp': node.timestamp,
                'changeset': node.changeset,
                'uid': node.uid,
                'user': node.user}


@dataclass(eq=True, frozen=True, order=True, unsafe_hash=True)
class osm_way:
    """
    Class for keeping track of buildings information
    """
    id: str
    version: str
    timestamp: str
    changeset: str
    uid: str
    user: str
    tag: list
    nodes: list
    nearest_edge: Edge
    centroid: Point
    approx_area: str
    type: int = field(default=BuildingType.BUILDING)

    @staticmethod
    def of_type(way, building_type: int):
        return osm_way(id=way.id,
                       version=way.version,
                       timestamp=way.timestamp,
                       changeset=way.changeset,
                       uid=way.uid,
                       user=way.user,
                       tag=way.tag,
                       nodes=way.nodes,
                       nearest_edge=way.nearest_edge,
                       centroid=way.centroid,
                       approx_area=way.approx_area,
                       type=building_type)

    @staticmethod
    def serializable(way):
        return {'id': way.id,
                'version': way.version,
                'timestamp': way.timestamp,
                'changeset': way.changeset,
                'uid': way.uid,
                'user': way.user,
                'tag': way.tag,
                'nodes': list(map(lambda node: osm_node.serialize(node), way.nodes)),
                'nearest_edge': {'id': way.nearest_edge[0].getID(),
                                 'dist': way.nearest_edge[1]} if way.nearest_edge else {},
                'centroid': str(way.centroid),
                'approx_area': way.approx_area,
                'type': way.type}

    @staticmethod
    def deserialize(way_dict, net):
        # deserialize nodes
        nodes = []
        for dd in way_dict['nodes']:
            nodes.append(osm_node(id=dd['id'],
                                  lat=dd['lat'],
                                  lon=dd['lon'],
                                  version=dd['version'],
                                  timestamp=dd['timestamp'],
                                  changeset=dd['changeset'],
                                  uid=dd['uid'],
                                  user=dd['user']))
        # deserialize nearest edge
        if way_dict['nearest_edge']:
            the_nearest_edge = (net.getEdge(way_dict['nearest_edge']['id']), way_dict['nearest_edge']['dist'])
        else:
            the_nearest_edge = None
        # return a new osm_way
        return osm_way(id=way_dict['id'],
                       version=way_dict['version'],
                       timestamp=way_dict['timestamp'],
                       changeset=way_dict['changeset'],
                       uid=way_dict['uid'],
                       user=way_dict['user'],
                       tag=way_dict['tag'],
                       nodes=nodes,
                       nearest_edge=the_nearest_edge,
                       centroid=Point(eval(way_dict['centroid'])),
                       approx_area=way_dict['approx_area'],
                       type=way_dict['type'])


class BuildingsFromOSM:
    """
    NOTE: some functions are taken from SAGA code (by Codeca L.)
    """

    def __init__(self, osm_fname, net_fname):
        self._osm_buildings = {}
        self.all_buildings = []  # All buildings in the input OSM
        self.schools = []
        self.apartments = []
        self.offices = []
        self.amenities = []
        self.other_buildings = []  # All buildings in the input OSM which have not been assigned

        self._net = sumolib.net.readNet(net_fname)
        if not config.TRY_LOAD_BUILDINGS_FROM_PREVIOUS_FILE:
            self._osm = self._parse_osm_xml_file(osm_fname)
            self._filter_buildings_from_osm()
        else:
            if path.exists("osm_buildings.pickle"):
                self.deserialize_buildings()
            else:
                self._osm = self._parse_osm_xml_file(osm_fname)
                self._filter_buildings_from_osm()
        if config.SERIALIZE_BUILDINGS:
            self.serialize_buildings()
        self.sort_buildings()

    def serialize_buildings(self, fname="osm_buildings.pickle"):
        ll = list(map(lambda building: osm_way.serializable(building), self.all_buildings))
        with open(fname, "wb") as pickle_out:
            pickle.dump(ll, pickle_out)

    def deserialize_buildings(self, fname="osm_buildings.pickle"):
        dict_pickle_in = pickle.load(open(fname, "rb"))
        for building in dict_pickle_in:
            self.all_buildings.append(osm_way.deserialize(building, self._net))

    def sort_buildings(self):
        """
        Sort buildings according to their type (school, apartments, ...) and assign them
        to specific data structures in this class.
        """
        schools = list(
            filter(lambda osm_way: self._is_osm_building_of_type(osm_way, 'school'), self.all_buildings))
        self.schools = list(map(lambda school: osm_way.of_type(school, BuildingType.SCHOOL), schools))

        offices = list(
            filter(lambda osm_way: self._is_osm_building_of_type(osm_way, 'office'), self.all_buildings))
        self.offices = list(map(lambda school: osm_way.of_type(school, BuildingType.OFFICE), offices))

        apartments = list(
            filter(lambda osm_way: self._is_osm_building_of_type(osm_way, 'apartments'), self.all_buildings))
        self.schools = list(map(lambda school: osm_way.of_type(school, BuildingType.APARTMENT), apartments))

        amenities = list(
            filter(lambda osm_way: self._is_osm_of_type(osm_way, 'amenity'), self.all_buildings))
        self.schools = list(map(lambda school: osm_way.of_type(school, BuildingType.AMENITY), amenities))

        schools_id = set(map(lambda bb: bb.id, self.schools))
        office_id = set(map(lambda bb: bb.id, self.offices))
        apartments_id = set(map(lambda bb: bb.id, self.apartments))
        amenities_id = set(map(lambda bb: bb.id, self.amenities))
        all_id = set(map(lambda bb: bb.id, self.all_buildings))
        all_id = all_id.difference(schools_id)
        all_id = all_id.difference(office_id)
        all_id = all_id.difference(apartments_id)
        all_id = all_id.difference(amenities_id)
        self.other_buildings = list(
            filter(lambda osm_way: osm_way.id in all_id, self.all_buildings))

    @staticmethod
    def _is_osm_building_of_type(osm_way, type):
        """Return if a way is a building"""
        for tag in osm_way.tag:
            if tag["k"] == "building" and tag["v"] == type:
                return True
        return False

    @staticmethod
    def _is_osm_of_type(osm_way, type):
        """Return if a way is a building"""
        for tag in osm_way.tag:
            if tag["k"] == type:
                return True
        return False

    @staticmethod
    def _get_centroid(nodes):
        points = []
        for node in nodes:
            lat, lon = float(node.lat), float(node.lon)
            points.append([lat, lon])
        centroid = numpy.mean(numpy.array(points), axis=0)
        return centroid

    @staticmethod
    def _get_approx_area(net, nodes):
        # compute the approximated area
        proj_points = []
        for node in nodes:
            lat, lon = float(node.lat), float(node.lon)
            proj_points.append(net.convertLonLat2XY(lon, lat))
        approx = geometry.MultiPoint(proj_points).convex_hull
        area = 0.0
        if not numpy.isnan(approx.area):
            area = approx.area
        return area

    @staticmethod
    def osm_way_to_dataclass(net, way):
        nodes = []
        for node_dict in way['nodes']:
            node = osm_node(node_dict['id'], node_dict['lat'], node_dict['lon'], node_dict['version'], node_dict[
                'timestamp'], node_dict['changeset'], \
                            node_dict['uid'], node_dict['user'])
            nodes.append(node)

        radius = 50
        centroid = BuildingsFromOSM._get_centroid(nodes)
        lat, lon = centroid
        x_coord, y_coord = net.convertLonLat2XY(lon, lat)
        neighbours = net.getNeighboringEdges(x_coord, y_coord, r=radius)
        nearest_edge = None
        if neighbours:
            sorted_neighbours = sorted(neighbours, key=lambda x: x[1])
            nearest_edge = sorted_neighbours[0]

        return osm_way(id=way['id'],
                       version=way['version'],
                       timestamp=way['timestamp'],
                       changeset=way['changeset'],
                       uid=way['uid'],
                       user=way['user'],
                       tag=way['tag'],
                       nodes=nodes,
                       nearest_edge=nearest_edge,
                       centroid=Point(centroid),
                       approx_area=BuildingsFromOSM._get_approx_area(net, nodes))

    def _filter_buildings_from_osm(self):
        def _is_building(way):
            """Return if a way is a building"""
            if "tag" not in way:
                return False
            for tag in way["tag"]:
                if tag["k"] == "building":
                    return True
            return False

        """
        Extract buildings from OSM structure.
        """
        nodes = dict()
        for node in tqdm(self._osm["node"]):
            nodes[node["id"]] = node
        for way in tqdm(self._osm["way"]):
            if not _is_building(way):
                continue
            building_dict = way
            building_dict["nodes"] = list()
            for ndid in way["nd"]:
                building_dict["nodes"].append(nodes[ndid["ref"]])
            self.all_buildings.append(self.osm_way_to_dataclass(self._net, building_dict))
        print("Found {} buildings.".format(len(self.all_buildings)))

    @classmethod
    def _parse_osm_xml_file(cls, xml_file):
        """
        Extract all info from an OSM file (OSM->dict)
        """
        xml_tree = xml.etree.ElementTree.parse(xml_file).getroot()
        dict_xml = {}
        for child in xml_tree:
            parsed = {}
            for key, value in child.attrib.items():
                parsed[key] = value

            for attribute in child:
                if attribute.tag in list(parsed.keys()):
                    parsed[attribute.tag].append(attribute.attrib)
                else:
                    parsed[attribute.tag] = [attribute.attrib]

            if child.tag in list(dict_xml.keys()):
                dict_xml[child.tag].append(parsed)
            else:
                dict_xml[child.tag] = [parsed]
        return dict_xml
