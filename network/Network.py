import time

import sumolib
import random
from tqdm import tqdm
from conf import config
from environment.OSM.BuildingsFromOSM import BuildingsFromOSM, osm_way
from routing.RoutingEdge import RoutingEdge
import numpy as np

class Network(object):
    f"""
    simply ready the network in its raw form and creates a router on this network

    NOTE: this code comes from TRAPP. Please, for troubleshooting contact:\n
    -> Ilias Gerostathopoulos, gerostat@in.tum.de\n
    -> Evangelos Pournaras, epournaras@ethz.ch
    """

    # empty references to start with
    edges = None
    nodes = None
    nodeIds = None
    edgeIds = None
    passenger_edges = None
    routingEdges = None
    routingEdgesList = None  # type: List[RoutingEdge]
    net = None
    osm_buildings = None  # type: BuildingsFromOSM

    @classmethod
    def loadNetwork(cls):
        """ loads the network and applies the results to the Network static class """
        if not config.scenario.net_path:
            parsedNetwork = sumolib.net.readNet(config.scenario.cfg_path)
        else:
            parsedNetwork = sumolib.net.readNet(config.scenario.net_path)

        # apply parsing to the network
        Network.__applyNetwork(parsedNetwork)
        random.seed(time.time())

    @classmethod
    def load_buildings_from_osm(cls):
        cls.osm_buildings = BuildingsFromOSM(config.scenario.osm_map, config.scenario.net_path)

    @classmethod
    def assign_buildings_to_tazs(cls, env):
        for building in tqdm(cls.osm_buildings.all_buildings):
            for region in env.regions:
                if region.contains_node_xy(building.centroid):
                    region.buildings.append(building)

    @classmethod
    def evaluate_avg_edges_length(cls, env):
        for region in tqdm(env.regions):
            edge_lengths = []
            for edge in region.edges:
                edge_lengths.append(edge.getLength())
            region.avg_axes_length = np.average(edge_lengths)

    @classmethod
    def assign_edges_to_tazs(cls, env):
        for edge in tqdm(cls.net.getEdges()):
            for region in env.regions:
                if region.contains_edge(edge.getID()):
                    region.edges.append(edge)

    @classmethod
    def __applyNetwork(cls, net):
        """ internal method for applying the values of a SUMO map """
        cls.nodeIds = map(lambda x: x.getID(), net.getNodes())  # type: list[str]
        cls.edgeIds = map(lambda x: x.getID(), net.getEdges(withInternal=False))  # type: list[str]
        cls.nodes = net.getNodes()
        cls.edges = net.getEdges(withInternal=False)
        cls.passenger_edges = [e for e in net.getEdges(withInternal=False) if e.allows("passenger")]
        cls.routingEdges = map(lambda x: RoutingEdge(x), cls.passenger_edges)
        cls.routingEdgesList = list(map(lambda x: RoutingEdge(x), cls.passenger_edges))
        cls.net = net

    @classmethod
    def nodesCount(cls):
        """ count the nodes """
        return len(cls.nodes)

    @classmethod
    def edgesCount(cls):
        """ count the edges """
        return len(cls.edges)

    @classmethod
    def getEdgeFromNode(cls, edge):
        return edge.getFromNode()

    @classmethod
    def getEdgeByID(cls, edgeID):
        return [x for x in cls.edges if x.getID() == edgeID][0]

    @classmethod
    def getEdgeIDsToNode(cls, edgeID):
        return cls.getEdgeByID(edgeID).getToNode()

    # returns the edge position of an edge
    @classmethod
    def getPositionOfEdge(cls, edge):
        return edge.getFromNode().getCoord()  # @todo average two

    @classmethod
    def get_edge_from_random_region(cls, building_type=0):

        edge = random.choice(Network.passenger_edges)
        return edge.getFromNode().getCoord()

    @classmethod
    def get_random_edge_position(cls):
        edge = random.choice(Network.passenger_edges)
        return edge.getFromNode().getCoord()

    @classmethod
    def get_random_node_id_of_passenger_edge(cls):
        edge = random.choice(Network.passenger_edges)
        return edge.getFromNode().getID()

    @classmethod
    def get_random_from_node_school(cls):
        building = random.choice(cls.osm_buildings.schools)  # type:osm_way
        return building.nearest_edge[0].getFromNode().getID()

    @classmethod
    def get_random_from_node_apartment(cls):
        building = random.choice(cls.osm_buildings.apartments)  # type:osm_way
        return building.nearest_edge[0].getFromNode().getID()

    @classmethod
    def get_random_from_node_office(cls):
        building = random.choice(cls.osm_buildings.offices)  # type:osm_way
        return building.nearest_edge[0].getFromNode().getID()
