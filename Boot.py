from colorama import Fore, Style

from Simulation.simulation import Simulation
from conf import config
from conf.config import scenario
from environment.Region import Region
from network.Network import Network
from routing.CustomRouter import CustomRouter
from util import SUMOConnector

if __name__ == "__main__":
    print(Fore.YELLOW)
    print('#####################################' + Fore.YELLOW)
    print('#             Starting...           #' + Fore.YELLOW)
    print('#####################################' + Fore.RESET)
    SUMOConnector.start(SUMOConnector.DEFAULT_SUMO_PORT)
    print(Fore.GREEN + f"# SUMO started correctly on port {SUMOConnector.DEFAULT_SUMO_PORT}!" + Fore.RESET)
    sim = Simulation()
    sim.create_environment()

    print(Fore.GREEN + "# Parsing OSM..." + Fore.RESET)
    Network.loadNetwork()

    if config.LOAD_REGIONS_FROM_FILE:
        print(Fore.GREEN + "# Loading Regions Data from File..." + Fore.RESET)
        regions = Region.deserialize_regions(Network.net)
        for region in regions:
            Simulation.env.regions.add(region)
    else:
        print(Fore.GREEN + "# Parsing Regions Data..." + Fore.RESET)
        Network.load_buildings_from_osm()
        print(Fore.GREEN + "# Assigning buildings to TAZs..." + Fore.RESET)
        Network.assign_buildings_to_tazs(sim.env)
        print(Fore.GREEN + "# Assigning edges to TAZs..." + Fore.RESET)
        Network.assign_edges_to_tazs(sim.env)
        print(Fore.GREEN + "# calculating per-region average edges length..." + Fore.RESET)
        Network.evaluate_avg_edges_length(sim.env)
    if config.SERIALIZE_REGIONS:
        Region.serialize_regions(Simulation.env.regions)

    print(Fore.GREEN + "# OK! " + Fore.RESET)
    print(
        Fore.CYAN + "# Nodes: " + str(Network.nodesCount()) + " / Edges: " + str(Network.edgesCount()) + Fore.RESET)

    # After the network is loaded, we init the router
    """CustomRouter.init()
    scenario.load()
    if config.USE_FIXED_NUM_VEHICLES:
        sim.add_vehicles()  # create random vehicles
    print(Fore.GREEN + "# Simulation initialized. Beginning loop  #" + Fore.RESET)
    sim.loop()"""

    SUMOConnector.close()
    print(Fore.GREEN + "# OK" + Fore.RESET)
