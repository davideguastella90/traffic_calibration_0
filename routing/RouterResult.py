class RouterResult:
    """Defines the result of our customRouter

    NOTE: this code comes from TRAPP. Please, for troubleshooting contact:\n
    -> Ilias Gerostathopoulos, gerostat@in.tum.de\n
    -> Evangelos Pournaras, epournaras@ethz.ch"""

    def __init__(self, tuple_param, is_victim):
        # is this a victim route
        self.is_victim = is_victim
        # the list of nodes to drive to
        self.node_list = tuple_param[0]
        # meta information for the route
        self.meta = tuple_param[1]
        # the route as list of edgeIDs
        self.route = list(map(lambda x: x['edgeID'], self.meta))  # type: list[str]
        # the cost for this route per edge
        self.costs = tuple_param[2]
        # the total cost for this route
        self.total_cost = tuple_param[3]

    def __str__(self):
        return "Routing(" + str(self.route) + "," + str(self.total_cost) + ")"
