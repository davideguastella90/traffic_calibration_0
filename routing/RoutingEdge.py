from util import Util


class RoutingEdge:
    """
    a wrapper we use to add information to an edge that we use for routing

    NOTE: this code comes from TRAPP. Please, for troubleshooting contact:\n
    -> Ilias Gerostathopoulos, gerostat@in.tum.de\n
    -> Evangelos Pournaras, epournaras@ethz.ch
    """

    # how important old data can be at maximum (x times old + 1 times new value)
    edge_average_influence = 140

    def __init__(self, edge):
        """ init the edge based on a SUMO edge """
        # the edgeID
        self.id = edge.getID()
        # the number of lanes
        self.lanes = edge.getLanes()
        # the maximum speed on this edge
        self.max_speed = edge.getSpeed()
        # the length in meter on this edge
        self.length = edge.getLength()
        # the node this edge is from
        self.from_node = edge.getFromNode()
        # the node this edge is to
        self.to_node = edge.getToNode()
        # the ID of the fromNode
        self.from_node_id = edge.getFromNode().getID()
        # the ID of the toNode
        self.to_node_id = edge.getToNode().getID()
        # the theoretical duration for this edgeb based on speed and length
        self.predicted_duration = self.length / self.max_speed
        # averages for the duration measured in the simulation
        self.average_duration = self.predicted_duration
        # how many averageDurations are already measured
        self.average_duration_counter = 0
        # when did we get the last infromation
        self.last_duration_update_tick = 0

    def apply_edge_duration_to_average(self, duration, tick):
        """ adds a duration to drive on this edge to the calculation """
        # VARIANTE 1
        # # if a hugh traffic happened some time ago, the new values should be more important
        old_data_influence = max(1, self.edge_average_influence - (tick - self.last_duration_update_tick))
        # # self.averageDurationCounter += 1 -- not used
        self.average_duration = Util.addToAverage(old_data_influence, self.average_duration, duration)
        # # print(str(oldDataInfluence))
        self.last_duration_update_tick = tick

        # VARIANTE 2
        # self.averageDurationCounter += 1
        # self.averageDuration = addToAverage(self.averageDurationCounter, self.averageDuration, duration)
        # self.lastDurationUpdateTick = tick

    def __str__(self):
        return "Edge(" + self.from_node.getID() \
            + "," + self.to_node.getID() \
            + "," + str(len(self.lanes)) \
            + "," + str(self.max_speed) \
            + "," + str(self.length) + ")"
