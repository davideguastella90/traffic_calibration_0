from random import gauss

from dijkstar import Graph, find_path

from network.Network import Network
from routing.RouterResult import RouterResult


class CustomRouter:
    """
    our own custom defined router

    NOTE: this code comes from TRAPP. Please, for troubleshooting contact:\n
    -> Ilias Gerostathopoulos, gerostat@in.tum.de\n
    -> Evangelos Pournaras, epournaras@ethz.ch
    """

    # Empty starting references
    edgeMap = None
    graph = None

    # the percentage of smart cars that should be used for exploration
    explorationPercentage = 0.0  # INITIAL JSON DEFINED!!!
    # randomizes the routes
    routeRandomSigma = 0.2  # INITIAL JSON DEFINED!!!
    # how much speed influences the routing
    maxSpeedAndLengthFactor = 1  # INITIAL JSON DEFINED!!!
    # multiplies the average edge value
    averageEdgeDurationFactor = 1  # INITIAL JSON DEFINED!!!
    # how important it is to get new data
    freshnessUpdateFactor = 10  # INITIAL JSON DEFINED!!!
    # defines what is the oldest value that is still a valid information
    freshnessCutOffValue = 500.0  # INITIAL JSON DEFINED!!!
    # how often we reroute cars
    reRouteEveryTicks = 20  # INITIAL JSON DEFINED!!!

    @classmethod
    def init(cls):
        """ set up the router using the already loaded network """
        cls.graph = Graph()
        cls.edgeMap = {}
        for edge in Network.routingEdges:
            cls.edgeMap[edge.id] = edge
            cls.graph.add_edge(edge.from_node_id, edge.to_node_id,
                               {'length': edge.length, 'maxSpeed': edge.max_speed,
                                'lanes': len(edge.lanes), 'edgeID': edge.id})

    @classmethod
    def minimal_route(cls, from_node, dest_node):
        """creates a minimal route based on length / speed  """
        cost_func = lambda u, v, e, prev_e: e['length'] / e['maxSpeed']
        route = find_path(cls.graph, from_node, dest_node, cost_func=cost_func)
        return RouterResult(route, False)

    @classmethod
    def route(cls, from_node, dest_node, tick):
        """ creates a route from the f(node) to the t(node) """
        # 1) SIMPLE COST FUNCTION
        # cost_func = lambda u, v, e, prev_e: max(0,gauss(1, CustomRouter.routeRandomSigma) \
        #                                         * (e['length']) / (e['maxSpeed']))

        # if car.victim:
        #     # here we reduce the cost of an edge based on how old our information is
        #     print("victim routing!")
        #     cost_func = lambda u, v, e, prev_e: (
        #         cls.getAverageEdgeDuration(e["edgeID"]) -
        #         (tick - (cls.edgeMap[e["edgeID"]].lastDurationUpdateTick))
        #     )
        # else:
        # 2) Advanced cost function that combines duration with averaging
        # isVictim = ??? random x percent (how many % routes have been victomized before)

        # isVictim = cls.explorationPercentage > random()
        is_victim = False

        if is_victim:
            victimization_choice = 1
        else:
            victimization_choice = 0

        cost_func = lambda u, v, e, prev_e: \
            cls.getFreshness(e["edgeID"], tick) * \
            cls.averageEdgeDurationFactor * \
            cls.getAverageEdgeDuration(e["edgeID"]) \
            + \
            (1 - cls.getFreshness(e["edgeID"], tick)) * \
            cls.maxSpeedAndLengthFactor * \
            max(1, gauss(1, cls.routeRandomSigma) *
                (e['length']) / e['maxSpeed']) \
            - \
            (1 - cls.getFreshness(e["edgeID"], tick)) * \
            cls.freshnessUpdateFactor * \
            victimization_choice

        # generate route
        route = find_path(cls.graph, from_node, dest_node, cost_func=cost_func)
        # wrap the route in a result object
        return RouterResult(route, is_victim)

    @classmethod
    def getFreshness(cls, edge_id, tick):
        try:
            last_update = float(tick) - cls.edgeMap[edge_id].last_duration_update_tick
            return 1 - min(1, max(0, last_update / cls.freshnessCutOffValue))
        except TypeError as e:
            # print("error in getFreshnessFactor" + str(e))
            return 1

    @classmethod
    def getAverageEdgeDuration(cls, edgeID):
        """ returns the average duration for this edge in the simulation """
        try:
            return cls.edgeMap[edgeID].average_duration
        except:
            print("error in getAverageEdgeDuration")
            return 1

    @classmethod
    def applyEdgeDurationToAverage(cls, edge, duration, tick):
        """ tries to calculate how long it will take for a single edge """
        try:
            cls.edgeMap[edge].apply_edge_duration_to_average(duration, tick)
        except:
            return 1

    @classmethod
    def route_by_max_speed(cls, fr, to):
        """ creates a route from the f(node) to the t(node) """
        cost_func = lambda u, v, e, prev_e: (1 / e['maxSpeed'])
        route = find_path(cls.graph, fr, to, cost_func=cost_func)
        return RouterResult(route, False)

    @classmethod
    def route_by_min_length(cls, fr, to):
        """ creates a route from the f(node) to the t(node) """
        cost_func = lambda u, v, e, prev_e: (e['length'])
        route = find_path(cls.graph, fr, to, cost_func=cost_func)
        return RouterResult(route, False)

    @classmethod
    def calculate_length_of_route(cls, route):
        return sum([cls.edgeMap[e].length for e in route])
