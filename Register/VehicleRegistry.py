import random

import traci
import traci.constants as tc

from Entity.Vehicle import NullVehicle, Vehicle
from conf import config


class VehicleRegistry:
    """ Registry for all vehicles we have in the sumo simulation """

    # always increasing counter for vehicleIDs
    veh_index_counter = 0
    # list of all vehicles
    vehicles = {}  # type: dict[str, Vehicle]
    # list of arrived vehicles
    arrived_vehicles = {}  # type: dict[str, Vehicle]

    vehicles_behavior = {}  # type: dict[str, bool] #bool is true whether the vehicle has an aggressive behavior

    # counts the number of finished trips
    total_trips = 0
    # average of all trip durations
    total_trip_average = 0
    # average of all trip overheads (overhead is TotalTicks/PredictedTicks)
    total_trip_overhead_average = 0

    def __init__(self, ):
        # start listening to all vehicles that arrived at their target
        traci.simulation.subscribe((tc.VAR_ARRIVED_VEHICLES_IDS,))

    @classmethod
    def apply_vehicle_counter(cls):
        """Syncs the value of the vehicleCounter to the SUMO simulation"""
        while len(VehicleRegistry.vehicles) < config.TOTAL_VEHICLE_COUNTER:
            # few vehicles -> add new
            new_vehicle = Vehicle("vehicle-" + str(VehicleRegistry.veh_index_counter))
            cls.veh_index_counter += 1
            if new_vehicle.add_to_simulation(0):
                cls.vehicles[new_vehicle.id] = new_vehicle
        while len(VehicleRegistry.vehicles) > config.TOTAL_VEHICLE_COUNTER:
            # to many vehicles -> remove vehicles until number is equal to
            # the specified number of vehicles in config file
            VehicleRegistry.vehicles.popitem()

    @classmethod
    def check_arrived_vehicles(cls):
        """
        Check for arrived vehicles (completed their assigned route) and
        re-add them into the simulation
        """
        for arrived_veh_id in traci.simulation.getArrivedIDList():
            vehicle = VehicleRegistry.find_by_id(arrived_veh_id)
            if not isinstance(vehicle, NullVehicle):
                cls.total_trips += 1
                # c.setArrived(cls.tick)
                cls.arrived_vehicles[vehicle.id] = vehicle
                del cls.vehicles[vehicle.id]
            cls.apply_vehicle_counter()

    @classmethod
    def set_vehicle_aggressive_behavior(cls, veh_id):
        """
        Called only when config.SIMULATE_COLLISIONS is true. This method
        specify an aggressive behavior for the vehicle which ID (sumo id) is
        given in input.

        :param veh_id: the SUMO ID of a vehicle
        :return:
        """
        traci.vehicle.setAccel(veh_id, 10)
        traci.vehicle.setDecel(veh_id, 4)
        traci.vehicle.setEmergencyDecel(veh_id, 0.1)
        traci.vehicle.setApparentDecel(veh_id, 0.1)
        traci.vehicle.setTau(veh_id, 0.01)
        traci.vehicle.setImperfection(veh_id, 0.1)
        traci.vehicle.setLaneChangeMode(veh_id, 0)

    @classmethod
    def do_vehicles_sim_step(cls, tick):
        """Perform a simulation step for all vehicles into the simulation.
        This consists of verifying the arrived vehicles, then performing a
        simulation step for vehicles not arrived yed (thus, still in simulation).
        :param tick: current time instant"""

        # check here if current vehicles arrived to their destination. If so, move arrived vehicles
        # from "cars" to "arrived_cars"
        if config.USE_FIXED_NUM_VEHICLES:
            cls.check_arrived_vehicles()
        else:
            # here, the number of vehicles is not established a priori in the configuration file
            for arrived_car_id in traci.simulation.getArrivedIDList():
                del cls.vehicles[arrived_car_id]
            for car_id in traci.vehicle.getIDList():
                if car_id not in cls.vehicles:
                    cls.veh_index_counter += 1
                    traci.vehicle.subscribe(car_id, config.CARS_SUBSCRIPTIONS)
                    cls.vehicles[car_id] = Vehicle(car_id)

                # if a behavior (aggressive or not) has not been specified for the vehicle
                # add an aggressive behavior with probability
                if car_id not in cls.vehicles_behavior and config.SIMULATE_COLLISIONS:
                    # PROBABILITY_AGGRESSIVE_BEHAVIOR
                    if random.random() < config.PROBABILITY_AGGRESSIVE_BEHAVIOR:
                        cls.set_vehicle_aggressive_behavior(car_id)
                        cls.vehicles_behavior[car_id] = True
                        # specify a color for aggressive vehicles
                        traci.vehicle.setColor(car_id, [0, 255, 255, 255])
                    else:
                        cls.vehicles_behavior[car_id] = False

        # do a simulation step for selected vehicles
        for car in cls.vehicles.values():
            car.on_simstep_performed(tick)

    @classmethod
    def find_by_id(cls, vehicle_id):
        """ returns a car by a given carID """
        try:
            return cls.vehicles[vehicle_id]
        except:
            return NullVehicle()
