class Scenario:
    def __init__(self):
        self.cfg_path = ""  # type: String
        self.net_path = ""  # type: String
        self.poly_path = ""  # type: String
        self.sim_range = []  # type: range
        self.osm_map = ""

    def load(self):
        pass


class Bruxelles(Scenario):
    def __init__(self):
        super().__init__()
        self.cfg_path = ""
        self.net_path = "Scenarios/BXL_big/bxl.net.xml"
        self.osm_map = "Scenarios/BXL_big/map_big.osm"
        self.poly_path = "Scenarios/BXL_big/taz.id.poly.simple.xml"
        self.sim_range = range(1, 400000)

    def load(self):
        print("loaded")


class Bruxelles_small(Scenario):
    def __init__(self):
        super().__init__()
        self.cfg_path = ""
        self.net_path = "Scenarios/BXL_small/bxl.net.xml"
        self.osm_map = "Scenarios/BXL_small/map_small.osm"
        self.poly_path = "Scenarios/BXL_small/taz.id.poly.simple.xml"
        self.sim_range = range(1, 400000)

    def load(self):
        print("loaded")
