from conf.scenarios import Bruxelles, Bruxelles_small
import traci.constants as tc

scenario = Bruxelles()

USE_RANDOMNESS = True

USE_SUMO_UI = False

# if true, a fixed number of vehicles are added into the simulation,
# and their path are generated randomly
USE_FIXED_NUM_VEHICLES = True

# the total amount of cars that should be in the system
TOTAL_VEHICLE_COUNTER = 400

VERBOSE_LOG = False
VERBOSE_LOG_DEVICES_EXCHANGE = False & VERBOSE_LOG

DATETIME_STR_FORMAT = "%d/%m/%Y %H:%M:%S"

# SUMO data types that all vehicles subscribe to
CARS_SUBSCRIPTIONS = (tc.VAR_POSITION,  # 0x42
                      tc.VAR_SPEED,  # 0x40
                      tc.VAR_ACCELERATION,  # 0x46
                      tc.VAR_ANGLE,  # 0x43
                      tc.VAR_CO2EMISSION,  # 0x60
                      tc.VAR_LANE_ID)  # 0x51

TRY_LOAD_BUILDINGS_FROM_PREVIOUS_FILE = False
SERIALIZE_BUILDINGS = False

SERIALIZE_REGIONS = True
LOAD_REGIONS_FROM_FILE = False



# ------------------------------------------------------------------------
#                       COLLISION CONFIGURATION
# ------------------------------------------------------------------------

# if true, some vehicles will have an aggressive behavior, causing accidents
SIMULATE_COLLISIONS = False

# When simulating collisions, new vehicles (that is, vehicles added into the simulation)
# are assigned an aggressive behavior with the probability value specified by this variable
PROBABILITY_AGGRESSIVE_BEHAVIOR = 0.5

COLLISION_MINGAP_FACTOR = 1  # default -1
COLLISION_ACTION = 'warn'  # [none,warn,teleport,remove]; default: teleport
COLLISION_STOPTIME = 80
